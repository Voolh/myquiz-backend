<?php namespace Auth;


use ApiTester;

class UserLogoutCest
{
    public function _before(ApiTester $I)
    {
        $I->setUriHeaders();
        $I->sendGET('airlock/csrf-cookie');
        $I->seeResponseCodeIs(204);

        $I->setUriHeaders();
        $I->setControlHeaders();
        $I->setPreviousCookies();
    }

    /**
     * @test
     * @group auth
     */
    public function unauthenticatedUserCanNotLogout(ApiTester $I)
    {
        $I->sendPOST('logout');
        $I->seeResponseCodeIs(401);
        $I->canSeeResponseContainsJson([
            "message" => "Unauthenticated.",
        ]);
    }

    /**
     * @test
     * @group auth
     */
    public function authenticatedUserCanLogoutSuccessfully(ApiTester $I)
    {
        $I->sendPOST('login', ['email' => 'test@test.tld', 'password' => 'password']);
        $I->seeResponseCodeIs(204);

        $I->setUriHeaders();
        $I->setControlHeaders();
        $I->setPreviousCookies();
        $I->sendPOST('logout');
        $I->seeResponseCodeIs(204);
    }
}
