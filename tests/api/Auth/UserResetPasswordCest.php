<?php namespace Auth;


use ApiTester;

class UserResetPasswordCest
{
    public function _before(ApiTester $I)
    {
        $I->setUriHeaders();
        $I->sendGET('airlock/csrf-cookie');
        $I->seeResponseCodeIs(204);

        $I->setUriHeaders();
        $I->setControlHeaders();
        $I->setPreviousCookies();
    }


    /**
     * @test
     * @group password
     */
    public function userCanPerformRequestToChangePasswordLink(ApiTester $I)
    {
        $I->sendPOST('password/email', ['email' => 'test@test.tld']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"message":"We have emailed your password reset link!"}');
    }

    /**
     * @test
     * @group password
     */
    public function userCanPerformRequestToChangePasswordWithToken(ApiTester $I)
    {
        $I->sendPOST('password/reset', [
            'token' => 'not_valid_token',
            'email' => 'test@test.tld',
            'password' => $newPassword = 'new_password',
            'password_confirmation' => $newPassword
        ]);
        $I->seeResponseCodeIs(422);
        $I->seeResponseIsJson();
        $I->seeResponseContains('"message":"The given data was invalid.","errors":{"email":["This password reset token is invalid."]}');
    }

    /**
     * @test
     * @group password
     */
    public function userCanNotRequestToChangePasswordLinkWithWrongEmail(ApiTester $I)
    {
        $I->sendPOST('password/email', ['email' => 'wrong_user@test.tld']);
        $I->seeResponseCodeIs(422);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"message":"The given data was invalid.","errors":{"email":["We can\'t find a user with that email address."]}}');
    }

}
