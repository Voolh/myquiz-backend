<?php namespace Auth;


use ApiTester;
use Faker\Factory as Faker;

class UserRegisterCest
{
    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function _before(ApiTester $I)
    {
        $this->faker = Faker::create();
        $I->setUriHeaders();
        $I->sendGET('airlock/csrf-cookie');
        $I->seeResponseCodeIs(204);

        $I->setUriHeaders();
        $I->setControlHeaders();
        $I->setPreviousCookies();
    }

    /**
     * @test
     * @group register
     */
    public function userCanRegisterWithRightCredentials(ApiTester $I)
    {
        $name = $this->faker->userName;
        $email = $this->faker->safeEmail;
        $password = $this->faker->password;

        $I->sendPOST('register', [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $password
        ]);

        $I->seeResponseCodeIs(201);
    }

    /**
     * @test
     * @group register
     */
    public function userCanNotRegisterTwice(ApiTester $I)
    {
        $name = 'Test User';
        $email = 'test@test.tld';
        $password = 'password';

        $I->sendPOST('register', [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $password
        ]);
        $I->seeResponseCodeIs(422);
        $I->seeResponseIsJson();
        $I->canSeeResponseContainsJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "email" => ["The email has already been taken."]
            ]
        ]);
    }
}
