<?php namespace Auth;


use ApiTester;

class UserLoginCest
{
    public function _before(ApiTester $I)
    {
    }

    /**
     * @test
     * @group auth
     */
    public function clientCanReceiveXsrfToken(ApiTester $I)
    {
        $I->sendGET('airlock/csrf-cookie');
        $I->seeResponseCodeIs(204);
        $I->seeCookie('XSRF-TOKEN');
    }

    /**
     * @test
     * @group auth
     */
    public function clientWillReceiveCsrfTokenMismatchOnLoginRoute(ApiTester $I)
    {
        $I->sendPOST('login');
        $I->seeResponseCodeIs(419);
    }

    public function clientCanNotLoginWithActualCsrfTokenAndWrongCredentials(ApiTester $I)
    {
        $I->setUriHeaders();
        $I->sendGET('airlock/csrf-cookie');
        $I->seeResponseCodeIs(204);

        $I->setUriHeaders();
        $I->setControlHeaders();
        $I->setPreviousCookies();
        $I->sendPOST('login', ['email' => 'wrong@test.tld', 'password' => 'wrong']);
        $I->seeResponseCodeIs(422);
    }

    /**
     * @test
     * @group auth
     */
    public function clientCanLoginWithActualCsrfTokenAndActualCredentials(ApiTester $I)
    {
        $I->setUriHeaders();
        $I->sendGET('airlock/csrf-cookie');
        $I->seeResponseCodeIs(204);

        $I->setUriHeaders();
        $I->setControlHeaders();
        $I->setPreviousCookies();
        $I->sendPOST('login', ['email' => 'test@test.tld', 'password' => 'password']);
        $I->seeResponseCodeIs(204);
    }

    /**
     * @test
     * @group auth
     */
    public function clientCanRecieveUserDataAsLoggedUser(ApiTester $I)
    {
        $I->setUriHeaders();
        $I->sendGET('airlock/csrf-cookie');
        $I->seeResponseCodeIs(204);

        $I->setUriHeaders();
        $I->setControlHeaders();
        $I->setPreviousCookies();
        $I->sendPOST('login', ['email' => 'test@test.tld', 'password' => 'password']);
        $I->seeResponseCodeIs(204);

        $I->setControlHeaders();
        $I->setPreviousCookies();
        $I->sendGet('api/user');
        $I->seeResponseCodeIs(200);
    }
}
