<?php namespace Auth;


use ApiTester;

class UserVerifyEmailCest
{
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $password;

    public function _before(ApiTester $I)
    {

        $this->email = 'test@test.tld';

        $this->password = 'password';

        $I->setUriHeaders();
        $I->sendGET('airlock/csrf-cookie');
        $I->seeResponseCodeIs(204);

        $I->setUriHeaders();
        $I->setControlHeaders();
        $I->setPreviousCookies();
    }

    /**
     * @test
     * @group email
     */
    public function userCanPerformRequestForVerifyEmail(ApiTester $I)
    {
        $confirmUrl = '/email/verify/31cdd1c3-683e-4fc7-a2cf-326516006a32/3c4cebb2a67ef157ad6f7b431a1ac087f6640912?expires=1590489212&signature=2bfed64c84b047aab4ca37e49af90efca4db192fa706fc52580d77f5529eeeae';

        $I->sendPOST('login', ['email' => $this->email, 'password' => $this->password]);
        $I->seeResponseCodeIs(204);

        $I->sendGET($confirmUrl);
        $I->seeResponseCodeIs(403);
        $I->canSeeResponseContains('"message": "Invalid signature."');
    }

    /**
     * @test
     * @group email
     */
    public function userCanPerformRequestForResendVerificationEmail(ApiTester $I)
    {
        $I->sendPOST('login', ['email' => $this->email, 'password' => $this->password]);
        $I->seeResponseCodeIs(204);

        $I->setUriHeaders();
        $I->setControlHeaders();
        $I->setPreviousCookies();
        $I->sendPOST('email/resend');
        $I->seeResponseCodeIs(202);
    }
}
