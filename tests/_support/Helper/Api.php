<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Api extends \Codeception\Module
{

const HEADER_HOST = 'api.myquiz.test';
const HEADER_ORIGIN = 'http://myquiz.test';
const HEADER_REFERER = 'http://myquiz.test:8081';

    /**
     * Store current cookies, and set them after browser history cleaning.
     * This action needs because the default behavior will set the HTTP_REFERER same as the current URI
     * if the browser history has a previous steps. Manipulation with cookies are needed because the 'REST'
     * module does not have a public method for clean only history.
     * @throws \Codeception\Exception\ModuleException
     */
    public function setPreviousCookies()
    {
        $cookieJar = $this->getModule('REST')->client->getCookieJar()->all();
        $this->clearHistory();
        foreach ($cookieJar as $cookie) {
            $this->getModule('REST')->client->getCookieJar()->set($cookie);
        }
    }

    /**
     * It flushes browser history and all cookies
     * @throws \Codeception\Exception\ModuleException
     */
    public function clearHistory()
    {
        $this->getModule('REST')->client->restart();
    }

    public function setUriHeaders()
    {
        $I = $this->getModule('PhpBrowser');
        $I->haveHttpHeader('Host', self::HEADER_HOST);
        $I->haveHttpHeader('Origin', self::HEADER_ORIGIN);
        $I->haveHttpHeader('Referer', self::HEADER_REFERER);
    }

    public function setControlHeaders()
    {
        $I = $this->getModule('PhpBrowser');
        $I->haveHttpHeader('Accept', 'application/json, text/plain, */*');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Connection', 'keep-alive');
        $I->haveHttpHeader('Pragma', 'no-cache');
        $I->haveHttpHeader('Cache-Control', 'no-cache');
        $I->haveHttpHeader('X-Requested-With', 'XMLHttpRequest');
        $I->haveHttpHeader('X-XSRF-TOKEN', $I->grabCookie('XSRF-TOKEN'));
    }
}
