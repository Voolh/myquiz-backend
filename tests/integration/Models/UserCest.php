<?php namespace integration\Models;


use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Schema;
use IntegrationTester;

class UserCest
{
    public function _before(IntegrationTester $I)
    {
        $this->faker = Faker::create();
    }

    /**
     * @test
     * @group models
     */
    public function usersTableHasExpectedColumns(IntegrationTester $I)
    {
        $I->assertTrue(Schema::hasColumns('users', [
            'id', 'name', 'email', 'email_verified_at', 'password', 'remember_token', 'created_at', 'updated_at'
        ]), 'Schema is wrong');
    }

    /**
     * @test
     * @group models
     */
    public function userCanBeCreated(IntegrationTester $I)
    {
        $user = $I->have(User::class);
        $I->seeInDatabase('users', [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email
        ]);
    }

    /**
     * @test
     * @group models
     */
    public function userCanBeRetrieved(IntegrationTester $I){
        $user = $I->have(User::class);

        $request = User::find($user->id);

        $I->assertEquals($user->all(), $request->all());
    }

    /**
     * @test
     * @group models
     */
    public function userCanBeDeleted(IntegrationTester $I)
    {
        $user = $I->have(User::class);
        $user->delete();
        $I->cantSeeInDatabase('users', ['id' => $user->id]);
    }

    /**
     * @test
     * @group models
     */
    public function userCanBeUpdated(IntegrationTester $I)
    {
        $user = $I->have(User::class);
        $name = $this->faker->sentence(5);
        $user->update(['name' => $name]);
        $I->canSeeInDatabase('users', ['id' => $user->id, 'name' => $name]);
    }
}
