<?php namespace integration\Models\Answers;


use App\Models\Answers\Select;
use App\Models\Quiz;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Schema;
use IntegrationTester;

class SelectCest
{
    private $faker;
    private $quiz;
    private $user;

    public function _before(IntegrationTester $I)
    {
        $this->faker = Faker::create();
        $this->user = factory(User::class)->create();
        $this->quiz = factory(Quiz::class)
            ->state('answer_is_select')
            ->create([
                'user_id' => $this->user->id,
            ]);
    }

    /**
     * @test
     * @group models
     */
    public function selectsTableHasExpectedColumns(IntegrationTester $I)
    {
        $I->assertTrue(Schema::hasColumns('selects', [
            'id', 'quiz_id', 'answer_type', 'text', 'is_correct', 'created_at', 'updated_at'
        ]), 'Schema is wrong');
    }

    /**
     * @test
     * @group models
     */
    public function selectCanBeCreated(IntegrationTester $I)
    {
        $select = $I->have(Select::class, [
            'quiz_id' => $this->quiz->id,
            'answer_type' => $answerType = $this->faker->randomElement(
                ['ChooseOne', 'ChooseMany']
            ),
        ]);
        $I->seeInDatabase('selects', [
            'id' => $select->id,
            'text' => $select->text,
            'answer_type' => $answerType
        ]);
    }

    /**
     * @test
     * @group models
     */
    public function selectCanBeRetrieved(IntegrationTester $I)
    {
        $select = $I->have(Select::class, [
            'quiz_id' => $this->quiz->id,
            'answer_type' => $answerType = $this->faker->randomElement(
                ['ChooseOne', 'ChooseMany']
            ),
        ]);

        $request = Select::find($select->id);

        $I->assertEquals($select->all(), $request->all());
    }

    /**
     * @test
     * @group models
     */
    public function selectCanBeDeleted(IntegrationTester $I)
    {
        $select = $I->have(Select::class, [
            'quiz_id' => $this->quiz->id,
            'answer_type' => $answerType = $this->faker->randomElement(
                ['ChooseOne', 'ChooseMany']
            ),
        ]);
        $select->delete();
        $I->cantSeeInDatabase('selects', ['id' => $select->id]);
    }

    /**
     * @test
     * @group models
     */
    public function selectCanBeUpdated(IntegrationTester $I)
    {
        $select = $I->have(Select::class, [
            'quiz_id' => $this->quiz->id,
            'answer_type' => $answerType = $this->faker->randomElement(
                ['ChooseOne', 'ChooseMany']
            ),
        ]);
        $select->update([
            'text' => $newText = $this->faker->sentence(3),
            'answer_type' => $newAnswerType = $this->faker->randomElement(
                ['ChooseOne', 'ChooseMany']
            ),
            'is_correct' => $newIsCorrect = $this->faker->boolean,

        ]);
        $I->canSeeInDatabase('selects', [
            'id' => $select->id,
            'text' => $newText,
            'answer_type' => $newAnswerType,
            'is_correct' => $newIsCorrect,
        ]);
    }

    /**
     * @test
     * @group models
     */
    public function selectWillBeDeletedWhenQuizIsDeleted(IntegrationTester $I)
    {
        $select = $I->have(Select::class, [
            'quiz_id' => $this->quiz->id,
            'answer_type' => $answerType = $this->faker->randomElement(
                ['ChooseOne', 'ChooseMany']
            ),
        ]);
        $this->quiz->delete();
        $I->cantSeeInDatabase('selects', ['id' => $select->id]);
    }
}
