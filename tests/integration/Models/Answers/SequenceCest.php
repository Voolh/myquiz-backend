<?php namespace integration\Models\Answers;


use App\Models\Answers\Sequence;
use App\Models\Quiz;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Schema;
use IntegrationTester;

class SequenceCest
{
    private $faker;
    private $quiz;
    private $user;

    public function _before()
    {
        $this->faker = Faker::create();
        $this->user = factory(User::class)->create();
        $this->quiz = factory(Quiz::class)
            ->state('answer_is_sequence')
            ->create([
                'user_id' => $this->user->id,
            ]);
    }

    /**
     * @test
     * @group models
     */
    public function sequencesTableHasExpectedColumns(IntegrationTester $I)
    {
        $I->assertTrue(Schema::hasColumns('sequences', [
            'id', 'quiz_id', 'answer_type', 'text', 'order', 'created_at', 'updated_at'
        ]), 'Schema is wrong');
    }

    /**
     * @test
     * @group models
     */
    public function sequenceCanBeCreated(IntegrationTester $I)
    {
        $sequence = $I->have(Sequence::class, [
            'quiz_id' => $this->quiz->id,
            'answer_type' => $answerType = $this->faker->randomElement(
                ['FillGaps', 'SequenceFromOptions', 'CorrectSequence']
            ),
        ]);
        $I->seeInDatabase('sequences', [
            'id' => $sequence->id,
            'text' => $sequence->text,
            'answer_type' => $answerType
        ]);
    }

    /**
     * @test
     * @group models
     */
    public function sequenceCanBeRetrieved(IntegrationTester $I){
        $sequence = $I->have(Sequence::class, [
            'quiz_id' => $this->quiz->id,
            'answer_type' => $answerType = $this->faker->randomElement(
                ['FillGaps', 'SequenceFromOptions', 'CorrectSequence']
            ),
        ]);

        $request = Sequence::find($sequence->id);

        $I->assertEquals($sequence->all(), $request->all());
    }

    /**
     * @test
     * @group models
     */
    public function sequenceCanBeDeleted(IntegrationTester $I)
    {
        $sequence = $I->have(Sequence::class, [
            'quiz_id' => $this->quiz->id,
            'answer_type' => $answerType = $this->faker->randomElement(
                ['FillGaps', 'SequenceFromOptions', 'CorrectSequence']
            ),
        ]);
        $sequence->delete();
        $I->cantSeeInDatabase('sequences', ['id' => $sequence->id]);
    }

    /**
     * @test
     * @group models
     */
    public function sequenceCanBeUpdated(IntegrationTester $I)
    {
        $sequence = $I->have(Sequence::class, [
            'quiz_id' => $this->quiz->id,
            'answer_type' => $answerType = $this->faker->randomElement(
                ['FillGaps', 'SequenceFromOptions', 'CorrectSequence']
            ),
        ]);
        $sequence->update([
            'text' => $newText = $this->faker->sentence(3),
            'answer_type' => $newAnswerType = $this->faker->randomElement(
                ['FillGaps', 'SequenceFromOptions', 'CorrectSequence']
            ),
            'order' => $newOrder = $this->faker->numberBetween(0, 1),

        ]);
        $I->canSeeInDatabase('sequences', [
            'id' => $sequence->id,
            'text' => $newText,
            'answer_type' => $newAnswerType,
            'order' => $newOrder,
        ]);
    }

    /**
     * @test
     * @group models
     */
    public function sequenceWillBeDeletedWhenQuizIsDeleted(IntegrationTester $I)
    {
        $sequence = $I->have(Sequence::class, [
            'quiz_id' => $this->quiz->id,
            'answer_type' => $answerType = $this->faker->randomElement(
                ['FillGaps', 'SequenceFromOptions', 'CorrectSequence']
            ),
        ]);
        $this->quiz->delete();
        $I->cantSeeInDatabase('sequences', ['id' => $sequence->id]);
    }
}
