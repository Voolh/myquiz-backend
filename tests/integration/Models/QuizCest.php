<?php namespace integration\Models;


use App\Models\Quiz;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Schema;
use IntegrationTester;

class QuizCest
{
    private $user;

    public function _before(IntegrationTester $I)
    {
        $this->faker = Faker::create();
        $this->user = factory(User::class)->create();
    }

    /**
     * @test
     * @group models
     */
    public function quizzesTableHasExpectedColumns(IntegrationTester $I)
    {
        $I->assertTrue(Schema::hasColumns('quizzes', [
            'id', 'user_id', 'title', 'answer_model', 'created_at', 'updated_at'
        ]), 'Schema is wrong');
    }

    /**
     * @test
     * @group models
     */
    public function quizCanBeCreated(IntegrationTester $I)
    {
        $quiz = $I->have(Quiz::class, ['user_id' => $this->user->id]);
        $I->seeInDatabase('quizzes', [
            'id' => $quiz->id,
            'title' => $quiz->title,
            'answer_model' => $quiz->answer_model
        ]);
    }

    /**
     * @test
     * @group models
     */
    public function userCanBeRetrieved(IntegrationTester $I)
    {
        $quiz = $I->have(Quiz::class, ['user_id' => $this->user->id]);

        $request = Quiz::find($quiz->id);

        $I->assertEquals($quiz->all(), $request->all());
    }

    /**
     * @test
     * @group models
     */
    public function quizCanBeDelete(IntegrationTester $I)
    {
        $quiz = $I->have(Quiz::class, ['user_id' => $this->user->id]);
        $quiz->delete();
        $I->cantSeeInDatabase('quizzes', ['id' => $quiz->id]);
    }

    /**
     * @test
     * @group models
     */
    public function quizCanBeUpdated(IntegrationTester $I)
    {
        $quiz = $I->have(Quiz::class, ['user_id' => $this->user->id]);
        $quiz->update(['title' => $newTitle = $this->faker->sentence(3)]);
        $I->canSeeInDatabase('quizzes', ['id' => $quiz->id, 'title' => $newTitle]);
    }

    /**
     * @test
     * @group models
     */
    public function quizWillBeDeletedWhenUserIsDeleted(IntegrationTester $I)
    {
        $quiz = $I->have(Quiz::class, ['user_id' => $this->user->id]);
        $this->user->delete();
        $I->cantSeeInDatabase('quizzes', ['id' => $quiz->id]);
    }
}
