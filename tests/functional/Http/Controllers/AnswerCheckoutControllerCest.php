<?php namespace Http\Controllers;


use App\Models\Answers\Select;
use App\Models\Quiz;
use App\Models\User;
use Faker\Factory as Faker;
use FunctionalTester;

class AnswerCheckoutControllerCest
{
    /**
     * @var \Faker\Generator
     */
    private $faker;
    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private $user;
    /**
     * @var array
     */
    private $requestData;

    public function _before(FunctionalTester $I)
    {
        $this->faker = Faker::create();
        $this->user = \factory(User::class)->state('has_id')->create();
        $I->amLoggedAs($this->user);
        $I->haveHttpHeader('Accept', 'application/json, text/plain, */*');
    }

    /**
     * @test
     * @group controller
     */
    public function postRequestWithRightAnswersForCheckoutQuizWillReturnSuccessfullyResponse(FunctionalTester $I)
    {
        $quiz = \factory(Quiz::class)->states('has_id', 'answer_is_select')
            ->create(['user_id' => $this->user->id])
            ->toArray();
        $answers = \factory(Select::class, 2)
            ->state('type_is_ChooseOne')
            ->create(['quiz_id' => $quiz['id']])
            ->each(function ($model, $i) {
                $model->is_correct = $i === 0 ?? true;
                $model->save();
            });

        $answers = $answers->toArray();

        $request = compact('quiz', 'answers');

        $I->sendAjaxRequest('PUT', 'answer-checkouts/'.$quiz['id'], $request);
        $I->seeResponseCodeIsSuccessful();
        $I->canSeeResponseContainsJson(['isCorrect' => true]);
    }

    /**
     * @test
     * @group controllers
     */
    public function postRequestWithWrongAnswersForCheckoutQuizWillReturnSuccessfullyResponse(FunctionalTester $I)
    {
        $quiz = \factory(Quiz::class)->states('has_id', 'answer_is_select')
            ->create(['user_id' => $this->user->id])
            ->toArray();
        $answers = \factory(Select::class, 2)
            ->state('type_is_ChooseOne')
            ->create(['quiz_id' => $quiz['id']])
            ->each(function ($model, $i) {
                $model->is_correct = $i === 0 ?? true;
                $model->save();
            });

        $answers = $answers->toArray();
        $answers[0]['is_correct'] = !$answers[0]['is_correct'];
        $answers[1]['is_correct'] = !$answers[1]['is_correct'];

        $request = compact('quiz', 'answers');

        $I->sendAjaxRequest('PUT', 'answer-checkouts/'.$quiz['id'], $request);
        $I->seeResponseCodeIsSuccessful();
        $I->canSeeResponseContainsJson(['isCorrect' => false]);
    }
}
