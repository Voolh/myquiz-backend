<?php namespace functional\Http\Controllers\Auth;

use FunctionalTester;

class RegisterControllerCest
{
    /**
     * @test
     * @group auth
     * @group controllers
     */
    public function registerRequest(FunctionalTester $I)
    {
        $I->haveHttpHeader('Accept', 'application/json, text/plain, */*');
        $I->sendAjaxPostRequest('register', [
            'name' => 'Bob',
            'email' => 'bob@example.org',
            'password' => $password = 'password',
            'password_confirmation' => $password
        ]);
        $I->seeResponseCodeIs(201);
    }
}
