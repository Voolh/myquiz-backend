<?php namespace functional\Http\Controllers;


use App\Models\Answers\Select;
use App\Models\Quiz;
use App\Models\User;
use Faker\Factory as Faker;
use FunctionalTester;

class QuizControllerCest
{

    private $user;
    private $faker;

    public function _before(FunctionalTester $I)
    {
        $this->faker = Faker::create();
        $this->user = \factory(User::class)->state('has_id')->create();
        $I->amLoggedAs($this->user);
        $I->haveHttpHeader('Accept', 'application/json, text/plain, */*');
    }

    private function fakeRequestCreator(): array
    {
        return $this->requestData = [
            'quiz' => [
                'title' => $this->faker->sentence(3),
                'answer_model' => 'Select',
            ],
            'answers' => [
                [
                    'text' => $this->faker->sentence(3),
                    'is_correct' => true,
                    'answer_type' => 'ChooseMany',
                ],
                [
                    'text' => $this->faker->sentence(3),
                    'is_correct' => false,
                    'answer_type' => 'ChooseMany',
                ],
            ]
        ];
    }

    /**
     * @test
     * @group controller
     */
    public function postRequestForStoringNewQuizWillReturnSuccessfullResponse(FunctionalTester $I)
    {
        $I->sendAjaxRequest('POST', 'quizzes', $this->fakeRequestCreator());
        $I->seeResponseCodeIsSuccessful();
    }

    /**
     * @test
     * @group controller
     */
    public function putRequestForUpdatingQuizWillReturnSuccessfullResponse(FunctionalTester $I)
    {
        $quiz = \factory(Quiz::class)->state('has_id')
            ->create(['user_id' => $this->user->id]);
        \factory(Select::class, 2)->state('type_is_ChooseOne')
            ->create(['quiz_id' => $quiz->id]);
        $I->sendAjaxRequest('PUT', 'quizzes/'.$quiz->id, $this->fakeRequestCreator());

        $I->seeResponseCodeIsSuccessful();
    }

    /**
     * @test
     * @group controller
     */
    public function getRequestForRetrievingQuizWillReturnSuccessfulResponse(FunctionalTester $I)
    {
        $quiz = \factory(Quiz::class)->state('has_id')
            ->create(['user_id' => $this->user->id]);

        $answers = \factory(Select::class)->state('type_is_ChooseMany')
            ->create(['quiz_id' => $quiz->id]);

        $I->sendAjaxRequest('GET', 'quizzes/'.$quiz->id);

        $I->seeResponseCodeIsSuccessful();
    }

    /**
     * @test
     * @group controller
     */
    public function getRequestForRetrievingQuizWillReturnNotFoundIfQuizDoesNotExist(FunctionalTester $I)
    {
        $I->sendAjaxRequest('GET', 'quizzes/'.$this->faker->uuid);
        $I->canSeeResponseCodeIs(404);
    }

    /**
     * @test
     * @group controller
     */
    public function getRequestForRetrievingListOfQuizzesWillReturnSuccessfulResponse(FunctionalTester $I)
    {
        $I->sendAjaxRequest('GET', 'quizzes');
        $I->seeResponseCodeIsSuccessful();
    }

    /**
     * @test
     * @group controller
     */
    public function deleteRequestWillReturnSuccessfulResponse(FunctionalTester $I)
    {
        $quiz = \factory(Quiz::class)->state('has_id')
            ->create(['user_id' => $this->user->id]);
        $I->sendAjaxRequest('DELETE', 'quizzes/' . $quiz->id);
        $I->seeResponseCodeIsSuccessful();
    }

    /**
     * @test
     * @group controller
     */
    public function deleteRequestWillReturnWillReturnNotFoundIfQuizDoesNotExist(FunctionalTester $I)
    {
        $I->sendAjaxRequest('DELETE', 'quizzes/' . $this->faker->uuid);
        $I->canSeeResponseCodeIs(404);
    }
}
