<?php namespace unit\Models\Answers;


use App\Models\Answers\Select;
use App\Models\Quiz;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Str;
use UnitTester;

class SelectCest
{
    private $model;
    private $faker;

    public function _before(UnitTester $I)
    {
        $this->faker = Faker::create();
        $this->model = \Mockery::mock(Select::class)
            ->makePartial();
    }

    protected function setNativeModel()
    {
        $this->model = factory(Select::class)->make();
    }

    /**
     * @test
     * @group models
     * @group answers
     */
    public function modelIncrementingPropertyIsFalse(UnitTester $I)
    {
        $I->assertFalse($this->model->incrementing);
    }

    /**
     * @test
     * @group models
     * @group answers
     */
    public function modelSetUuidMethodHasInvokedWhenCreationMethodHasInvoked(UnitTester $I)
    {
        $this->model->expects('setUuid')
            ->withNoArgs()
            ->once();

        $this->model::bootUuid();
    }

    /**
     * @test
     * @group models
     * @group answers
     */
    public function theReturnedClosureOfSetUuidMethodWillSetIdAsUuid(UnitTester $I)
    {
        call_user_func($this->model::setUuid(), $this->model);

        $I->assertTrue(Str::isUuid($this->model->id));
    }

    /**
     * @test
     * @group models
     * @group answers
     */
    public function methodBootUuidHasInvokeCreatingMethodWithExpectedArguments(UnitTester $I)
    {
        $this->model->expects('creating')
            ->with(\Closure::class)
            ->once()
            ->andReturn(true);

        $this->model::bootUuid();
    }

    /**
     * @test
     * @group models
     * @group answers
     */
    public function fillableContainsAllExpectedFields(UnitTester $I)
    {
        $fillable = ["quiz_id", "answer_type", "text", "is_correct"];
        $actual = $this->model->getFillable();
        asort($fillable);
        asort($actual);
        $I->assertTrue(array_values($fillable) == array_values($actual));
    }

    /**
     * @test
     * @group models
     * @group answers
     * @before setNativeModel
     */
    public function modelShouldHaveQuizRelation(UnitTester $I)
    {
        $foreignKey = 'quiz_id';
        $relationship = $this->model->quiz();
        $relatedModel = $relationship->getRelated();
        $I->assertInstanceOf(BelongsTo::class, $relationship);
        $I->assertInstanceOf(Quiz::class, $relatedModel);
        $I->assertEquals($foreignKey, $relationship->getForeignKeyName());
    }
}
