<?php namespace unit\Models;

use App\Models\Quiz;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use Faker\Factory as Faker;
use UnitTester;

class UserCest
{
    private $model;
    private $faker;

    public function _before(UnitTester $I)
    {
        $this->model = \Mockery::mock(User::class)->makePartial();
        $this->faker = Faker::create();
    }

    protected function setNativeModel()
    {
        $this->model = factory(User::class)->state('has_id')->make();
    }

    /**
     * @test
     * @group models
     */
    public function modelIncrementingPropertyIsFalse(UnitTester $I)
    {
        $I->assertFalse($this->model->incrementing);
    }

    /**
     * @test
     * @group models
     */
    public function modelSetUuidMethodHasInvokedWhenCreationMethodHasInvoked(UnitTester $I)
    {
        $this->model->expects('setUuid')
            ->withNoArgs()
            ->once();

        $this->model::bootUuid();
    }

    /**
     * @test
     * @group models
     */
    public function theReturnedClosureOfSetUuidMethodWillSetIdAsUuid(UnitTester $I)
    {
        call_user_func($this->model::setUuid(), $this->model);

        $I->assertTrue(Str::isUuid($this->model->id));
    }

    /**
     * @test
     * @group models
     */
    public function methodBootUuidHasInvokeCreatingMethodWithExpectedArguments(UnitTester $I)
    {
        $this->model->expects('creating')
            ->with(\Closure::class)
            ->once()
            ->andReturn(true);

        $this->model::bootUuid();
    }

    /**
     * @test
     * @group models
     */
    public function fillableContainsAllExpectedFields(UnitTester $I)
    {
        $fillable = ['name', 'email', 'password'];
        $actual = $this->model->getFillable();
        asort($fillable);
        asort($actual);
        $I->assertTrue(array_values($fillable) == array_values($actual));
    }

    /**
     * @test
     * @group models
     * @before setNativeModel
     */
    public function modelShouldHaveQuizzesRelation(UnitTester $I)
    {
        $foreignKey = 'user_id';
        $relationship = $this->model->quizzes();
        $relatedModel = $relationship->getRelated();
        $I->assertInstanceOf(HasMany::class, $relationship);
        $I->assertInstanceOf(Quiz::class, $relatedModel);
        $I->assertEquals($foreignKey, $relationship->getForeignKeyName());
    }
}
