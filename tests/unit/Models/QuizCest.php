<?php namespace unit\Models;


use App\Models\Answers\Select;
use App\Models\Answers\Sequence;
use App\Models\Quiz;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use UnitTester;

class QuizCest
{
    private $model;
    private $faker;

    public function _before(UnitTester $I)
    {
        $this->faker = Faker::create();
        $this->model = \Mockery::mock(Quiz::class)
            ->makePartial();
    }

    protected function setNativeModel()
    {
        $this->model = factory(Quiz::class)->state('has_id')->make();
    }

    /**
     * @test
     * @group models
     */
    public function modelIncrementingPropertyIsFalse(UnitTester $I)
    {
        $I->assertFalse($this->model->incrementing);
    }

    /**
     * @test
     * @group models
     */
    public function modelSetUuidMethodHasInvokedWhenCreationMethodHasInvoked(UnitTester $I)
    {
        $this->model->expects('setUuid')
            ->withNoArgs()
            ->once();

        $this->model::bootUuid();
    }

    /**
     * @test
     * @group models
     */
    public function fillableContainsAllExpectedFields(UnitTester $I)
    {
        $fillable = ['title', 'user_id', 'answer_model'];
        $actual = $this->model->getFillable();
        asort($fillable);
        asort($actual);
        $I->assertTrue(array_values($fillable) == array_values($actual));
    }

    /**
     * @test
     * @group models
     */
    public function theReturnedClosureOfSetUuidMethodWillSetIdAsUuid(UnitTester $I)
    {
        call_user_func($this->model::setUuid(), $this->model);

        $I->assertTrue(Str::isUuid($this->model->id));
    }

    /**
     * @test
     * @group models
     */
    public function methodBootUuidHasInvokeCreatingMethodWithExpectedArguments(UnitTester $I)
    {
        $this->model->expects('creating')
            ->with(\Closure::class)
            ->once()
            ->andReturn(true);

        $this->model::bootUuid();
    }

    /**
     * @test
     * @group models
     * @before setNativeModel
     */
    public function modelShouldHaveUserRelation(UnitTester $I)
    {
        $foreignKey = 'user_id';
        $relationship = $this->model->user();
        $relatedModel = $relationship->getRelated();
        $I->assertInstanceOf(BelongsTo::class, $relationship);
        $I->assertInstanceOf(User::class, $relatedModel);
        $I->assertEquals($foreignKey, $relationship->getForeignKeyName());
    }

    /**
     * @test
     * @group models
     * @before setNativeModel
     */
    public function modelShouldHaveSelectsModelRelation(UnitTester $I)
    {
        $foreignKey = 'quiz_id';
        $relationship = $this->model->selects();
        $relatedModel = $relationship->getRelated();
        $I->assertInstanceOf(HasMany::class, $relationship);
        $I->assertInstanceOf(Select::class, $relatedModel);
        $I->assertEquals($foreignKey, $relationship->getForeignKeyName());
    }

    /**
     * @test
     * @group models
     * @before setNativeModel
     */
    public function quizShouldHaveSequencesModelRelation(UnitTester $I)
    {
        $foreignKey = 'quiz_id';
        $relationship = $this->model->sequences();
        $relatedModel = $relationship->getRelated();
        $I->assertInstanceOf(HasMany::class, $relationship);
        $I->assertInstanceOf(Sequence::class, $relatedModel);
        $I->assertEquals($foreignKey, $relationship->getForeignKeyName());
    }

    /**
     * @test
     * @group models
     * @before setNativeModel
     */
    public function quizCanReturnCorrespondingModelIfAnswerModelIsDefined(UnitTester $I)
    {
        $answerModel = 'Select';
        $this->model->answer_model = $answerModel;

        $relationship = $this->model->getAnswers();
        $relatedModel = $relationship->getRelated();

        $I->assertInstanceOf(HasMany::class, $relationship);
        $I->assertInstanceOf(Select::class, $relatedModel);
    }

    /**
     * @test
     * @group models
     * @before setNativeModel
     */
    public function quizShouldReturnCorrespondingModelIfAnswerModelIsDefined(UnitTester $I)
    {
        $answerModel = 'Select';
        $this->model->answer_model = $answerModel;

        $relationship = $this->model->getAnswers();
        $relatedModel = $relationship->getRelated();

        $I->assertInstanceOf(HasMany::class, $relationship);
        $I->assertInstanceOf(Select::class, $relatedModel);
    }

    /**
     * @test
     * @group models
     * @before setNativeModel
     */
    public function quizShouldReturnFalseIfAnswerModelIsNotDefined(UnitTester $I)
    {
        $relationship = $this->model->getAnswers();

        $I->assertFalse($relationship);
    }
}
