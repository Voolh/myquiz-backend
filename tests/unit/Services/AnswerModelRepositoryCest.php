<?php namespace unit\Services;

use App\Exceptions\Custom\InvalidObjectPropertyException;
use App\Models\Answers\Select;
use App\Models\Quiz;
use App\Models\User;
use App\Services\AnswerModelResolver;
use App\Services\Quiz\Answers\AnswerModelRepository;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use UnitTester;

class AnswerModelRepositoryCest
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var mixed
     */
    private $faker;
    /**
     * @var array
     */
    private $requestData;

    public function _before(UnitTester $I)
    {
        $this->faker = Faker::create();
        $this->user = factory(User::class)->state('has_id')->make();
        $I->amLoggedAs($this->user);
        $this->requestData = $this->fakeRequestCreator();
    }

    private function fakeRequestCreator(): array
    {
        return $this->requestData = [
            'quiz' => [
                'title' => $this->faker->sentence(3),
                'answer_model' => 'Select',
            ],
            'answers' => [
                [
                    'text' => $this->faker->sentence(3),
                    'is_correct' => true,
                    'answer_type' => 'ChooseOne',
                ],
                [
                    'text' => $this->faker->sentence(3),
                    'is_correct' => false,
                    'answer_type' => 'ChooseOne',
                ],
            ]
        ];
    }

    /**
     * @test
     * @group services
     * @group repository
     */
    public function throwExceptionWhenQuizDoesNotHaveIdProperty(UnitTester $I)
    {
        $request = new Request();
        $request->merge($this->requestData);

        $quizMock = \Mockery::mock(
            factory(Quiz::class)->make(['answer_model' => 'Select'])
        );

        $answerModelResolverMock = \Mockery::mock(AnswerModelResolver::class);

        $I->expectThrowable(
            new InvalidObjectPropertyException('Quiz instance does not have a valid id property'),
            function () use ($quizMock, $answerModelResolverMock, $request) {
                new AnswerModelRepository($quizMock, $answerModelResolverMock, $request);
            }
        );
    }


    /**
     * @test
     * @group services
     * @group repository
     * @throws \Exception
     */
    public function canStoreAnswers(UnitTester $I)
    {
        $request = new Request();
        $request->merge($this->requestData);

        $quizMock = \Mockery::mock(
            factory(Quiz::class)->states(['has_id'])->make(['answer_model' => 'Select'])
        );

        $selectModelMock = \Mockery::mock(factory(Select::class)->make());

        $selectModelMock
            ->expects('create')
            ->twice();

        $answerModelResolverMock = \Mockery::mock(AnswerModelResolver::class);

        $answerModelResolverMock
            ->expects('getAnswerModel')
            ->once()
            ->andReturn($selectModelMock);

        $answer = new AnswerModelRepository($quizMock, $answerModelResolverMock, $request);

        $answer->store();
    }

    /**
     * @group services
     * @group repository
     * @throws \Exception
     */
    public function canUpdateAnswers(UnitTester $I)
    {
        $request = new Request();
        $request->merge($this->requestData);

        $quizMock = \Mockery::mock(
            factory(Quiz::class)->states(['has_id'])->make(['answer_model' => 'Select'])
        );

        $selectModelMock = \Mockery::mock(factory(Select::class)->make());

        $selectModelMock
            ->expects('create')
            ->twice();

        $selectModelMock
            ->expects('where->delete')
            ->andReturn(true);

        $answerModelResolverMock = \Mockery::mock(AnswerModelResolver::class);

        $answerModelResolverMock
            ->expects('getAnswerModel')
            ->once()
            ->andReturn($selectModelMock);

        $answer = new AnswerModelRepository($quizMock, $answerModelResolverMock, $request);

        $answer->update();
    }
}
