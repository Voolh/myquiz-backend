<?php namespace unit\Services\QuizCheckout;


use App\DTO\QuizAnswerCheckoutDTO;
use App\Models\Answers\Select;
use App\Models\Quiz;
use App\Services\Quiz\QuizCheckout\Checkout;
use UnitTester;

class CheckoutCest
{


    /**
     * @test
     * @group business
     * @group checkout
     */
    public function checkoutReturnTrueIfAnswersIsCorrect(UnitTester $I, $scenario)
    {
        $quiz = factory(Quiz::class)
            ->states('has_id', 'answer_is_select')->make();

        $answerCollection = factory(Select::class, 2)
            ->states('type_is_ChooseOne')->make(['quiz_id' => $quiz->id])
            ->each(function ($model, $i) {
                $model->id = (string) \Str::uuid();
                $model->is_correct = $i === 0 ?? true;
            });

        $answerCheckoutDTO = new QuizAnswerCheckoutDTO([
            'quiz' => $quiz->toArray(),
            'answers' => $answerCollection->toArray()
        ]);

        $checkoutObj = new Checkout($answerCheckoutDTO, $answerCollection);

        $I->assertTrue($checkoutObj->isAnswerIsCorrect());
    }

    /**
     * @test
     * @group business
     * @group checkout
     */
    public function checkoutReturnTrueIfAnswersIsNotCorrect(UnitTester $I, $scenario)
    {
        $quiz = factory(Quiz::class)
            ->states('has_id', 'answer_is_select')->make();

        $answerCollection = factory(Select::class, 2)
            ->states('type_is_ChooseOne')->make(['quiz_id' => $quiz->id])
            ->each(function ($model, $i) {
                $model->id = (string) \Str::uuid();
                $model->is_correct = $i === 0 ?? true;
            });

        $dtoAnswers = $answerCollection->toArray();
        $dtoAnswers[0]['is_correct'] = !$dtoAnswers[0]['is_correct'];
        $dtoAnswers[1]['is_correct'] = !$dtoAnswers[1]['is_correct'];

        $answerCheckoutDTO = new QuizAnswerCheckoutDTO([
            'quiz' => $quiz->toArray(),
            'answers' => $dtoAnswers
        ]);

        $checkoutObj = new Checkout($answerCheckoutDTO, $answerCollection);

        $I->assertFalse($checkoutObj->isAnswerIsCorrect());
    }
}
