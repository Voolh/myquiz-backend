<?php namespace unit\Services;


use App\Exceptions\Custom\InvalidObjectPropertyException;
use App\Models\Answers\Select;
use App\Models\Answers\Sequence;
use App\Models\Quiz;
use App\Services\AnswerModelResolver;
use Illuminate\Contracts\Container\BindingResolutionException;
use UnitTester;

/**
 * Class AnswerModelResolverCest
 * @package unit\services
 */
class AnswerModelResolverCest
{
    /**
     * @test
     * @group services
     * @throws BindingResolutionException
     * @throws InvalidObjectPropertyException
     */
    public function canResolveSelectModel(UnitTester $I)
    {
        $quiz = factory(Quiz::class)
            ->states(['has_id', 'has_user_id', 'answer_is_select'])
            ->make();

        $resolver = new AnswerModelResolver($quiz);

        $I->assertInstanceOf(Select::class, $resolver->getAnswerModel());
    }

    /**
     * @test
     * @group services
     * @throws BindingResolutionException
     * @throws InvalidObjectPropertyException
     */
    public function canResolveSequenceModel(UnitTester $I)
    {
        $quiz = factory(Quiz::class)
            ->states(['has_id', 'has_user_id', 'answer_is_sequence'])
            ->make();

        $resolver = new AnswerModelResolver($quiz);

        $I->assertInstanceOf(Sequence::class, $resolver->getAnswerModel());
    }

    /**
     * @test
     * @group services
     */
    public function nullAnswer_ModelWillBeThrownException(UnitTester $I)
    {
        $quiz = factory(Quiz::class)
            ->states(['has_id', 'has_user_id'])
            ->make();

        $I->expectThrowable(
            InvalidObjectPropertyException::class,
            function () use ($quiz) {
                new AnswerModelResolver($quiz);
            }
        );
    }

    /**
     * @test
     * @group services
     */
    public function invalidAnswer_ModelWillBeThrownException(UnitTester $I)
    {
        $quiz = factory(Quiz::class)
            ->states(['has_id', 'has_user_id'])
            ->make(['answer_model' => 'User']);

        $I->expectThrowable(
            BindingResolutionException::class,
            function () use ($quiz) {
                new AnswerModelResolver($quiz);
            }
        );
    }
}
