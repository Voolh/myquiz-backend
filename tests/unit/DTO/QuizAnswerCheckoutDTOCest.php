<?php namespace DTO;


use App\DTO\QuizAnswerCheckoutDTO;
use App\Http\Requests\Checkout\AnswerCheckoutRequest;
use App\Models\Answers\Select;
use App\Models\Quiz;
use UnitTester;

class QuizAnswerCheckoutDTOCest
{
    /**
     * @test
     * @group DTO
     */
    public function canReceiveDtoFromValidatedRequest(UnitTester $I)
    {
        $quiz = factory(Quiz::class)
            ->states('has_id', 'has_timestamps', 'has_user_id', 'answer_is_select')
            ->make()->toArray();

        $answers = factory(Select::class, 2)
            ->states('has_id', 'type_is_ChooseOne', 'has_timestamps')
            ->make(['quiz_id' => $quiz['id']])->each(function ($model, $i) {
                $model->is_correct = $i === 0 ?? true;
            })->toArray();
        $requestMock = \Mockery::mock(AnswerCheckoutRequest::class);
        $requestMock->expects('validated')
            ->andReturn(compact('quiz', 'answers'));

        $dto = new QuizAnswerCheckoutDTO();
        $dto->fromValidatedRequest($requestMock);

        $I->assertSame($dto->quiz, \Arr::except($quiz, ['created_at', 'updated_at']));
        $I->assertSame($dto->answers, array_map(function (array $el) {
            $el['is_correct'] = $el['is_correct'] == true ? 1 : 0;
            return \Arr::except($el, ['created_at', 'updated_at']);
        }, $answers));
    }
}
