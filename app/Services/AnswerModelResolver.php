<?php


namespace App\Services;


use App\Exceptions\Custom\InvalidObjectPropertyException;
use App\Models\Quiz;
use App\Models\Answers\AnswerModel;
use Illuminate\Contracts\Container\BindingResolutionException;

/**
 * Class AnswerModelResolver
 * @package App\Services
 */
class AnswerModelResolver implements ModelResolver
{
    /**
     * @var mixed
     */
    private $answerModel;

    /**
     * AnswerModelResolver constructor.
     * @param Quiz $quiz
     * @throws BindingResolutionException
     * @throws InvalidObjectPropertyException
     */
    public function __construct(Quiz $quiz)
    {
        $this->answerModel = resolve($quiz->getAttribute('answer_model'));
        if (!($this->answerModel instanceof AnswerModel)) {
            throw new InvalidObjectPropertyException(
                'Invalid answer_model "' . $quiz->getAttribute('answer_model') . '" defined'
            );
        }
    }

    /**
     * @return AnswerModel
     */
    public function getAnswerModel(): AnswerModel
    {
        return $this->answerModel;
    }
}
