<?php


namespace App\Services\Quiz\QuizCheckout;

use App\DTO\QuizAnswerCheckoutDTO;
use App\Models\Answers\AnswerModel;
use Illuminate\Database\Eloquent\Collection;

class Checkout
{
    /**
     * @var array
     */
    private $answersFromUser;
    /**
     * @var Collection
     */
    private $answersDbCollection;

    /**
     * @var bool
     */
    private $isAnswerIsCorrect;


    /**
     * Checkout constructor.
     * @param QuizAnswerCheckoutDTO $answerCheckoutDTO
     * @param Collection $collection
     */
    public function __construct(QuizAnswerCheckoutDTO $answerCheckoutDTO, Collection $collection)
    {
        $this->answersFromUser = $answerCheckoutDTO->answers;
        $this->answersDbCollection = $collection;
        $this->isAnswerIsCorrect = true;
        $this->init();
    }

    private function init(): void
    {
        foreach ($this->answersDbCollection as $answer) {
            $key = $this->findEqualKey($answer);
            $answerWithoutTimestamps = $this->removeTimestamps($answer);

            if (!$this->isEqualAnswerArrays($answerWithoutTimestamps, $key)) {
                $this->isAnswerIsCorrect = false;
                break;
            }
        }
    }

    /**
     * Defines a key for identical answer elements
     * @param $answer
     * @return int
     */
    private function findEqualKey($answer): int
    {
        return array_search($answer->id, array_column($this->answersFromUser, 'id'), true);
    }

    /**
     * Remove timestamps from Db answers collection
     * @param AnswerModel $answer
     * @return array
     */
    private function removeTimestamps(AnswerModel $answer): array
    {
        return \Arr::except($answer->toArray(), ['created_at', 'updated_at']);
    }

    /**
     * Compare arrays from Db and user request
     * @param $dbAnswer
     * @param $key
     * @return bool
     */
    private function isEqualAnswerArrays($dbAnswer, $key): bool
    {
        return count(array_diff($dbAnswer, $this->answersFromUser[$key])) === 0 ? true : false;
    }

    /**
     * Answer validation result
     * @return bool
     */
    public function isAnswerIsCorrect(): bool
    {
        return $this->isAnswerIsCorrect;
    }
}
