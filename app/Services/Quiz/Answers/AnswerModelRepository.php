<?php


namespace App\Services\Quiz\Answers;

use App\Exceptions\Custom\InvalidObjectPropertyException;
use App\Models\Quiz;
use App\Services\ModelResolver;
use Illuminate\Http\Request;

/**
 * Class AnswerModelRepository
 * @package App\Services\Quiz\Answers
 */
class AnswerModelRepository implements ModelRepository
{
    private $quiz;
    private $answers;
    private $answerModel;

    /**
     * @inheritDoc
     * @throws InvalidObjectPropertyException
     */
    public function __construct(
        Quiz $quiz,
        ModelResolver $modelResolver,
        Request $request
    ) {
        if (empty($quiz->getAttribute('id'))) {
            throw new InvalidObjectPropertyException('Quiz instance does not have a valid id property');
        }
        $this->quiz = $quiz;
        $this->answerModel = $modelResolver->getAnswerModel();
        $this->answers = $request->get('answers');
    }

    /**
    * @inheritDoc
     */
    public function store(): void
    {
        foreach ($this->answers as $answer) {
            $answer['quiz_id'] = $this->quiz->id;
            $this->answerModel->create($answer);
        }
    }

    /**
     * @inheritDoc
     */
    public function update(): void
    {
        $this->delete();
        $this->store();
    }

    /**
     * Update answers for current quiz to db
     */
    private function delete(): void
    {
        $this->answerModel->where('quiz_id', $this->quiz->id)->delete();
    }
}
