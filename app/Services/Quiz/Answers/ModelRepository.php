<?php


namespace App\Services\Quiz\Answers;


use App\Models\Quiz;
use App\Services\ModelResolver;
use Illuminate\Http\Request;

interface ModelRepository
{
    /**
     * ModelRepository constructor.
     * @param Quiz $quiz
     * @param ModelResolver $modelResolver
     * @param Request $request
     */
    public function __construct(Quiz $quiz, ModelResolver $modelResolver, Request $request);

    /**
     * Store answers for current quiz to db
     */
    public function store(): void;

    /**
     * Update answers for current quiz to db
     */
    public function update(): void;
}
