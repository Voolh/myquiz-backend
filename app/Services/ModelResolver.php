<?php

namespace App\Services;

use App\Models\Quiz;
use App\Models\Answers\AnswerModel;

interface ModelResolver
{
    public function __construct(Quiz $quiz);

    /**
     * @return AnswerModel
     */
    public function getAnswerModel(): AnswerModel;
}
