<?php


namespace App\DTO;

use App\Http\Requests\Checkout\AnswerCheckoutRequest;
use Spatie\DataTransferObject\DataTransferObject;

final class QuizAnswerCheckoutDTO extends DataTransferObject
{
    /**
     * @var array
     */
    private $validated = [];

    /**
     * @var array
     */
    public $quiz = [];

    /**
     * @var array
     */
    public $answers = [];

    /**
     * @param AnswerCheckoutRequest $request
     */
    public function fromValidatedRequest(AnswerCheckoutRequest $request): void
    {
        $this->validated = $request->validated();

        $this->quiz = $this->getQuizArrayWithoutTimestamps();
        $this->answers = $this->getAnswersArrayWithoutTimestamps();

        if (in_array($this->quiz['answer_model'], ['Select', 'Sequence'])) {
            $this->answers = $this->changeBooleanToTinyInt($this->answers);
        }
    }

    /**
     * Remove timestamps from array
     * @return array
     */
    private function getQuizArrayWithoutTimestamps(): array
    {
        return \Arr::except($this->validated['quiz'], ['created_at', 'updated_at']);
    }

    /**
     * Remove timestamps from array
     * @return array
     */
    private function getAnswersArrayWithoutTimestamps(): array
    {
        return array_map(function (array $el) {
            return \Arr::except($el, ['created_at', 'updated_at']);
        }, $this->validated['answers']);
    }


    /**
     * Set 'is_correct' value as tinyint
     * @param array $answers
     * @return array
     */
    private function changeBooleanToTinyInt(array $answers): array
    {
        return array_map(function (array $el) {
            $el['is_correct'] = $el['is_correct'] == true ? 1 : 0;
            return $el;
        }, $answers);
    }
}
