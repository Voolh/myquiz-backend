<?php

namespace App\Providers;

use App\Models\Answers\Select;
use App\Models\Answers\Sequence;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('Select', function ($app): Model {
            return new Select();
        });

        $this->app->bind('Sequence', function ($app): Model {
            return new Sequence();
        });
    }
}
