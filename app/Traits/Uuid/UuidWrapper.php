<?php


namespace App\Traits\Uuid;

use Illuminate\Support\Str;

/**
 * Trait UuidWrapper
 * This wrapper allow to generate an 'uuid' instead of incremental 'id'
 * @package App\Traits\Uuid
 */
trait UuidWrapper
{
    /**
     * Calls the 'creating' method to set the uuid assignment event when the record is saved to db
     */
    public static function bootUuid(): void
    {
        static::creating(static::setUuid());
    }


    /**
     * Preparing the closure for 'creating' event
     * @return callable
     */
    public static function setUuid(): callable
    {
        return static function ($model) {
            $model->id = (string) Str::uuid();
        };
    }
}
