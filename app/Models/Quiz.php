<?php

namespace App\Models;

use App\Models\Answers\Select;
use App\Models\Answers\Sequence;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid\UuidWrapper;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Quiz
 * @package App\Models
 */
class Quiz extends Model
{
    use UuidWrapper;

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @inheritDoc
     */
    public static function boot()
    {
        parent::boot();
        static::bootUuid();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'answer_model'
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function selects(): HasMany
    {
        return $this->hasMany(Select::class);
    }

    /**
     * @return HasMany
     */
    public function sequences(): HasMany
    {
        return $this->hasMany(Sequence::class);
    }

    public function getAnswers()
    {
        if (empty($this->answer_model)) {
            return false;
        }
        $relations = [
            'Select' => $this->selects(),
            'Sequence' => $this->sequences()
        ];

        return $relations[$this->answer_model];
    }
}
