<?php

namespace App\Models\Answers;

use App\Models\Quiz;
use App\Traits\Uuid\UuidWrapper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Select
 * @package App\Models\Answers
 */
class Select extends Model implements AnswerModel
{
    use UuidWrapper;

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @inheritDoc
     */
    public static function boot()
    {
        parent::boot();
        static::bootUuid();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quiz_id', 'answer_type', 'text', 'is_correct'
    ];

    /**
     * @return BelongsTo
     */
    public function quiz(): BelongsTo
    {
        return $this->belongsTo(Quiz::class);
    }
}
