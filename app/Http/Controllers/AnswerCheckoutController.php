<?php

namespace App\Http\Controllers;

use App\DTO\QuizAnswerCheckoutDTO;
use App\Http\Requests\Checkout\AnswerCheckoutRequest;
use App\Models\Quiz;
use App\Services\Quiz\QuizCheckout\Checkout;
use Illuminate\Http\JsonResponse;

class AnswerCheckoutController extends Controller
{
    /**
     * @var QuizAnswerCheckoutDTO
     */
    private $quizAnswerCheckoutDTO;

    /**
     * AnswerCheckoutController constructor.
     * @param QuizAnswerCheckoutDTO $quizAnswerCheckoutDTO
     */
    public function __construct(QuizAnswerCheckoutDTO $quizAnswerCheckoutDTO)
    {
        $this->quizAnswerCheckoutDTO = $quizAnswerCheckoutDTO;
    }

    /**
     * Checkout user's answer.
     *
     * @param AnswerCheckoutRequest $request
     * @param Quiz $quiz
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(AnswerCheckoutRequest $request, Quiz $quiz): JsonResponse
    {
        $this->quizAnswerCheckoutDTO->fromValidatedRequest($request);
        $dbAnswers = $quiz->getAnswers()->get();

        $checkout = new Checkout($this->quizAnswerCheckoutDTO, $dbAnswers);

        $isCorrect = $checkout->isAnswerIsCorrect();

        return response()->json(compact('isCorrect'), 200);
    }
}
