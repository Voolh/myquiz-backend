<?php

namespace App\Http\Controllers;

use App\Models\Quiz;
use App\Services\AnswerModelResolver;
use App\Services\Quiz\Answers\AnswerModelRepository;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    private $quiz;

    /**
     * QuizController constructor.
     * @param Quiz $quiz
     */
    public function __construct(Quiz $quiz)
    {
        $this->quiz = $quiz;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function store(Request $request)
    {
        $data = $request->get('quiz');
        $data['user_id'] = \Auth::user()->id;
        $quiz = $this->quiz->create($data);

        $answerModel = new AnswerModelResolver($quiz);

        $answerRepository = new AnswerModelRepository($quiz, $answerModel, $request);
        $answerRepository->store();

        return response([], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function show(Quiz $quiz)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function edit(Quiz $quiz)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Quiz $quiz
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \App\Exceptions\Custom\InvalidObjectPropertyException
     */
    public function update(Request $request, Quiz $quiz)
    {
        $quiz->update($request->get('quiz'));

        $answerModel = new AnswerModelResolver($quiz);

        $answerRepository = new AnswerModelRepository($quiz, $answerModel, $request);
        $answerRepository->update();

        return response([], 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quiz $quiz)
    {
        //
    }
}
