<?php

namespace App\Http\Requests\Checkout;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AnswerCheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quiz' => 'required|array|min:1',
            'quiz.id' => 'required|uuid',
            'quiz.title' => 'required|string',
            'quiz.answer_model' => ['required', Rule::in('Select', 'Sequence')],
            'answers' => 'required|array|min:1',
            'answers.*.text' => 'required|string',
            'answers.*.is_correct' => [Rule::requiredIf('quiz.answer_model' == 'Select'), 'boolean'],
            'answers.*.order' => [Rule::requiredIf('quiz.answer_model' == 'Sequence'), 'integer'],
            'answers.*.answer_type' => [
                'required',
                Rule::in(
                    'ChooseMany',
                    'ChooseOne',
                    'FillGaps',
                    'SequenceFromOptions',
                    'CorrectSequence'
                )],

        ];
    }
}
