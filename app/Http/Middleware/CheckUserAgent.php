<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;

class CheckUserAgent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->userAgent() === 'Symfony BrowserKit') {
            Config::set('app.name', 'MyQuizBackend_Test');
            Config::set('database.connections.mysql.host', 'dev-mariadb');
            Config::set('database.connections.mysql.database', 'test-myquiz');
            Config::set('session.cookie', 'myquizbackend_test_session');
            Config::set('mail.default', 'log');
        }

        return $next($request);
    }
}
