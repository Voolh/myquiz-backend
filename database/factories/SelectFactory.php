<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Answers\Select;
use Faker\Generator as Faker;

$factory->define(Select::class, function (Faker $faker) {
    return [
        'quiz_id' => null,
        'text' => $faker->sentence(2, true),
        'answer_type' => null,
        'is_correct' => true
    ];
});

$factory->state(Select::class, 'has_id', function (Faker $faker) {
    return [
        'id' => $faker->uuid,
    ];
});

$factory->state(Select::class, 'has_quiz_id', function (Faker $faker) {
    return [
        'quiz_id' => $faker->uuid,
    ];
});

$factory->state(Select::class, 'type_is_ChooseOne', function (Faker $faker) {
    return [
        'answer_type' => 'ChooseOne',
    ];
});

$factory->state(Select::class, 'type_is_ChooseMany', function (Faker $faker) {
    return [
        'answer_type' => 'ChooseMany',
    ];
});

$factory->state(Select::class, 'has_timestamps', function (Faker $faker) {
    return [
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
    ];
});
