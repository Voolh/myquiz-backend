<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Answers\Sequence;
use App\Models\Quiz;
use Faker\Generator as Faker;

$factory->define(Sequence::class, function (Faker $faker) {
    return [
        'quiz_id' => null,
        'text' => $faker->sentence(2, true),
        'answer_type' => null,
        'order' => $faker->numberBetween(0, 1)
    ];
});

$factory->state(Sequence::class, 'type is FillGaps', function (Faker $faker) {
    return [
        'answer_type' => 'FillGaps',
    ];
});

$factory->state(Sequence::class, 'type is SequenceFromOptions', function (Faker $faker) {
    return [
        'answer_type' => 'SequenceFromOptions',
    ];
});

$factory->state(Sequence::class, 'type is CorrectSequence', function (Faker $faker) {
    return [
        'answer_type' => 'CorrectSequence',
    ];
});
