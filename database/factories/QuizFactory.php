<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Quiz;
use Faker\Generator as Faker;

$factory->define(Quiz::class, function (Faker $faker) {
    return [
        'user_id' => null,
        'title' => $faker->sentence(5, true),
        'answer_model' => null
    ];
});

$factory->state(Quiz::class, 'has_id', function (Faker $faker) {
    return [
        'id' => $faker->uuid,
    ];
});

$factory->state(Quiz::class, 'has_user_id', function (Faker $faker) {
    return [
        'user_id' => $faker->uuid,
    ];
});

$factory->state(Quiz::class, 'answer_is_select', function (Faker $faker) {
    return [
        'answer_model' => 'Select',
    ];
});

$factory->state(Quiz::class, 'answer_is_sequence', function (Faker $faker) {
    return [
        'answer_model' => 'Sequence',
    ];
});

$factory->state(Quiz::class, 'has_timestamps', function (Faker $faker) {
    return [
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
    ];
});
