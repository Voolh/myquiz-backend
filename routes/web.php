<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::apiResource('quizzes', 'QuizController');

Route::put('answer-checkouts/{answer_checkout}', 'AnswerCheckoutController');

Route::post('login', 'Auth\LoginController@login')->name('login');
Route::middleware('auth')->post('logout', 'Auth\LoginController@logout')->name('logout');
// Password Reset Routes...
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset');

// Registration Routes
Route::post('register', 'Auth\RegisterController@register')->name('register');
// Verification Routes
Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::post('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

//User Dashboard
Route::middleware('verified')->group(function () {
    //
});
